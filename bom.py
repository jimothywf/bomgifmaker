#!/usr/bin/python

'''
Look up the BOM website (particularly this page http://www.bom.gov.au/products/IDR663.loop.shtml)
Download all the layers of the radar and procedurally generate a radar over time gif

All the other radars seem to follow the same naming convention, so won't be too hard to make it more dynamic
Output: IDR663.gif
'''

import os
import sys
import requests
import shutil
import re
from PIL import Image, ImageDraw, ImageFilter
import imageio

# This is the radar we're using
RADAR="IDR663"
fileList = ['IDR663.range.png','IDR663.topography.png','IDR663.locations.png','IDR663.background.png']
loopImgs = []

def getLoopImgs():
	baseurl="http://www.bom.gov.au"
	url="http://www.bom.gov.au/products/IDR663.loop.shtml"

	content = requests.get(url)

	if (content.status_code != 200):
		print("Status %s... Failed to GET %s" % (content.status_code, url))

	expression="(theImageNames\[\d\]\s=\s\")(/radar/IDR663\.\D\.\d+\.png)(\";)"

	img = re.findall(expression, (content.content).decode('utf-8'))

	for x in range(0, len(img)):
		localpath = img[x][1]
		fullurl = baseurl + localpath
		endfile = localpath[7:] # Dodgy, use an RE in future

		print("Downloading image loop %s" % (localpath))

		response = requests.get(fullurl, stream=True)
		with open(endfile, "wb") as out_file:
			shutil.copyfileobj(response.raw, out_file)
		del response

		# Keep a record of all files not made transparant
		loopImgs.append(endfile)
		print("Download of %s complete" % (localpath))



'''
Need to find all these images locally
http://www.bom.gov.au/products/radar_transparencies/IDR663.range.png
http://www.bom.gov.au/products/radar_transparencies/IDR663.topography.png
http://www.bom.gov.au/products/radar_transparencies/IDR663.locations.png
http://www.bom.gov.au/products/radar_transparencies/IDR663.background.png
'''
def verifyBackground():
	backgroundBaseUrl="http://www.bom.gov.au/products/radar_transparencies/"

	# Check that each file exists locally
	for x in range(0, len(fileList)):

		# File doesn't exist, download it
		if (os.path.exists(fileList[x]) == False):
			print("%s does not exist, downloading" % (fileList[x]))
			fullBackground = backgroundBaseUrl + fileList[x]
			response = requests.get(fullBackground, stream=True)
			with open(fileList[x], "wb") as out_file:
				shutil.copyfileobj(response.raw, out_file)
			del response
			print("Download of %s complete" % (fileList[x]))


'''
Stack an image on top of another
The radar pictures white space is actually black with 0 alpha (0,0,0,0), and not white (255,255,255,255)
'''
def stackImg(file1, file2, wob, outfile):
	img1 = Image.open(file1)
	img1 = img1.convert("RGBA")
	img1data = img1.getdata()

	img2 = Image.open(file2)
	img2 = img2.convert("RGBA")
	img2data = img2.getdata()

	newData = []
	# Go through every pixel
	for x in range(0, len(img1data)-1):
		
		if wob == "white":
			# If the pixel is white (255, 255, 255), use pixel from img1data, otherwise use img2data
			if img2data[x][0] == 255 and img2data[x][1] == 255 and img2data[x][2] == 255:
				newData.append((img1data[x][0], img1data[x][1], img1data[x][2], img1data[x][3]))
			else:
				newData.append((img2data[x][0], img2data[x][1], img2data[x][2], img2data[x][3]))

		elif wob == "black":
			# If the pixel is black with no alpha (0, 0, 0, 0), use pixel from img1data, otherwise use img2data
			if img2data[x][0] == 0 and img2data[x][1] == 0 and img2data[x][2] == 0 and img2data[x][3] == 0:
				newData.append((img1data[x][0], img1data[x][1], img1data[x][2], img1data[x][3]))
			else:
				newData.append((img2data[x][0], img2data[x][1], img2data[x][2], img2data[x][3]))

	img1.putdata(newData)
	img1.save(outfile,"PNG")


if __name__ == "__main__":
	
	# Verify we have all the background pictures
	print("Verifying background images")
	verifyBackground()

	# Get the images for our loop
	print("Downloading loop images")
	getLoopImgs()

	# Stack images on top of each other
	print("Generating base PNG - IDR633.png")
	stackImg("IDR663.background.png","IDR663.topography.png", "white", "IDR633.png")
	stackImg("IDR633.png", "IDR663.range.png", "white", "IDR633.png")
	stackImg("IDR633.png", "IDR663.locations.png", "white", "IDR633.png")

	# Generate individual frames
	frame = 1
	for x in loopImgs:
		print("Stacking %s" % (x))
		stackImg("IDR633.png", x, "black", str(frame)+".png")
		frame += 1
	
	# Generate the gif
	print("Generating gif")
	images = []
	for x in range(1, frame):
		# Do this more than once to slow it down
		images.append(imageio.imread(str(x)+".png"))
		images.append(imageio.imread(str(x)+".png"))
		images.append(imageio.imread(str(x)+".png"))
	imageio.mimsave("IDR633.gif", images)


	# Now clean up all the shit left behind by this disaster
	print("Tidying up the bullshit")
	for x in loopImgs:
		os.remove(x)

	for x in fileList:
		os.remove(x)

	for x in range(1, frame):
		os.remove(str(x)+".png")

	os.remove("IDR633.png")
	